--------------- ABOUT ---------------

The quickest and most lightweight WebM recorder on the web.

WEBBUM SNAP is a simple tool to capture WebM videos from any part of your screen.
Simply rescale the window to fit your desired size and hit RECORD.


--------------- INSTALLATION ---------------

Download the files from the "Downloads" folder on the left. WebbumSnap.exe and ffmpeg.exe must be saved to the same destination. 
Thats it. No further setup required. 


--------------- CONTACT ---------------

webbumsnap@gmail.com
